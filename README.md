# Pyomeca examples

<p><img src="https://raw.githubusercontent.com/pyomeca/biorbd_design/main/logo_png/biorbd_full.png" width="300px">
<img src="https://raw.githubusercontent.com/pyomeca/biorbd_design/main/logo_png/bioviz_full.png" width="300px"></p>

A colleciton of simple examples codes for simpler starting with  [pyomeca](https://github.com/pyomeca) solftware, particularly wiht [biorbd](https://github.com/pyomeca/biorbd) and [bioviz](https://github.com/pyomeca/bioviz)

## Download the examples

### By using the terminal:
> Make sure to clone the submodules as well

```bash
git clone --recurse-submodules https://gitlab.inria.fr/auctus-team/people/antunskuric/example/pyomeca_examples.git
```

### By zip download
1) Download this repository amd unzip it: 
https://gitlab.inria.fr/auctus-team/people/antunskuric/example/pyomeca_examples

2)  Download the repository of models and unzip it in the same folder: https://gitlab.inria.fr/auctus-team/components/modelisation/humanmodels/pyomeca_models

Your folder structure should be something like this:
```
pyomeca_examples
    ├── pyomeca_models
    |   |
    |   ├── BrasComplet_4DOF.bioMod
    |   ├── ...
    |   └── MOBL_ARMS_fixed_33_nohand.bioMod
    |
    ├──  test_ik_imus.py
    └──  ...
```
## Installing biorbd and bioviz
The simplest way to install these python libraries is using anaconda.
```bash
conda env create -f env.yaml    # create the new environemnt and install biorbd,bioviz 
conda actiavte biomech
```

### creating a custom envirnoment
You can also simply use anaconda to create a new custom environment:
```bash
conda create -n biomech python=3 # create python 3 based environment
conda activate biomech           # activate the environment 
conda install biorbd bioviz      # install biorbd and bioviz

# optionallly for the `test_visual_capacity_force.py`
pip install pycapacity 
```

## Running the examples
Open your terminal and activate your conda environment
```
conda activate biomech
```
Then you should be set and you should be able to run all the examples.
The simplest example is:
```bash
python test_visual.py # display the interactive musculoskeletal model
```

The most complete example is interactive force polytope calculation:
```bash
python test_visual_capacity_force.py  # interactive real-time force polytope calculation
```
